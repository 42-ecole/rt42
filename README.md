# RT 42
RT42 is a school 21 project, which implement's light behavior and represent it with different objects.

![](https://gitlab.com/xHisusx/rt42/-/raw/master/resources/images/image.png)

  - Use JSON to serve scene files. You can find properties description in scenes files.
  - Switch between Ray-Tracing and Path-Tracing by one button.
  - Save result in highly loaded scene.
  - Apply post-effect filters to your viewport. 

# How to use
There is only one way to use this project on your mac.
Download or clone this project to local machine.

#### Building for source
For build just type in console:
```
make
```
First, graphic library SDL will be unarchived, configured and build. It's may take a few minutes.
Then rt42 sources wil be compiled.

Finaly build generate file RT. Use follow command to run program.
```
./RT scenes/box.json
```

# Controls

There is keys for user interface.

| Key | Description |
| ------ | ------ |
| W, A, S, D, SPACE, TAB | Movements |
| Arrows | Camera rotation |
| P | Switch path tracing |
| Page Up, Page Down | +/- Brightness    |
| I | Save intermediate results |
| L | Load intermediate results |
| K | Switch auto-save mode |
| -, = | Inc./Dec. Blur effect|
| ESC | Exit |

You can choose one of the follow filters to make unique result scene.

| Key | Filter |
| ------ | ------ |
| 1 | Gray |
| 2 | Sepia |
| 3 | Negative |
| 4 | Blur |
| 5 | Median|