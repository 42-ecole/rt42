/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_tracing.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:56:31 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:56:32 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PATH_TRACING_H
# define PATH_TRACING_H

# include "rt.h"

typedef enum	e_probability_type
{
	ALBEDO,
	SPECULAR,
	REFRACTION,
}				t_probability_type;

int				fits_probability(double probability, t_material *material,
					t_probability_type type);
void			albedo_ray(t_ray *ray, t_intersection intersection,
					t_vector *signal);
void			specular_ray(t_ray *ray, t_intersection intersection,
					t_vector *signal);
void			refraction_ray(t_ray *ray, t_intersection intersection,
					t_vector *signal);

#endif
