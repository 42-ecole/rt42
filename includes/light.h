/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   light.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:57:02 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:57:03 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIGHT_H
# define LIGHT_H

# include "rt.h"
# include "geometry.h"

typedef struct	s_lighting
{
	t_scene			*scene;
	t_intersection	intersection;
	t_vector		view_direction;
	t_vector		normal;
	t_vector		point;
	double			specular_exponent;
	double			light_distance;
}				t_lighting;

void			point_light(t_lighting lighting, t_light light,
					double *diffuse, double *specular);
void			parallel_light(t_lighting lighting,
					t_light light, double *diffuse, double *specular);
void			directional_light(t_lighting lighting,
					t_light light, double *diffuse, double *specular);

#endif
