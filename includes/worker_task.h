/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   worker_task.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:56:37 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:56:38 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WORKER_TASK_H
# define WORKER_TASK_H

# include "rt.h"
# include "network_api.h"

typedef struct	s_thread_arg
{
	SDL_sem		*semaphore;
	t_render	*render;
	t_sample	*sample;
	size_t		spp;
}				t_thread_arg;

#endif
