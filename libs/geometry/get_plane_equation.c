/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_plane_equation.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:53:32 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:53:33 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_plane_equation	get_plane_equation(t_vector normal, t_vector point)
{
	t_plane_equation res;

	res.a = normal.x;
	res.b = normal.y;
	res.c = normal.z;
	res.d = -(normal.x * point.x + normal.y * point.y + normal.z * point.z);
	return (res);
}
