/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transpose_mat.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:54:29 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:54:30 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_4x4matrix	transpose_mat(t_4x4matrix matrix)
{
	size_t		i;
	size_t		j;
	t_4x4matrix	res;

	i = -1;
	res = matrix;
	while (++i < 4 && (j = -1))
		while (++j < 4)
		{
			if (i == j)
				j++;
			if (j == 4)
				break ;
			res.value[i][j] = matrix.value[j][i];
		}
	return (res);
}
