/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   identity_mat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:53:37 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:53:38 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_4x4matrix	identity_mat(void)
{
	t_4x4matrix res;
	unsigned	i;
	unsigned	j;

	i = -1;
	while (++i < 4 && (j = -1))
		while (++j < 4)
			res.value[i][j] = 0.;
	res.value[0][0] = 1.;
	res.value[1][1] = 1.;
	res.value[2][2] = 1.;
	res.value[3][3] = 1.;
	return (res);
}
