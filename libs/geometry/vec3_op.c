/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_op.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:54:40 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:58:17 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

double		dot(t_vector v1, t_vector v2)
{
	return ((v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z));
}

t_vector	cross(t_vector v1, t_vector v2)
{
	t_vector res;

	res.x = v1.y * v2.z - v1.z * v2.y;
	res.y = v1.z * v2.x - v1.x * v2.z;
	res.z = v1.x * v2.y - v1.y * v2.x;
	res.w = 0.;
	return (res);
}

t_vector	mul_vec3(t_vector vector, double number)
{
	vector.x *= number;
	vector.y *= number;
	vector.z *= number;
	return (vector);
}

t_vector	normal_vec3(t_vector vector)
{
	double	lenght;

	lenght = norm2(vector);
	if (lenght != 1. && lenght != 0.)
	{
		lenght = sqrt(lenght);
		vector.x /= lenght;
		vector.y /= lenght;
		vector.z /= lenght;
	}
	return (vector);
}

t_vector	sum_vec3(t_vector v1, t_vector v2)
{
	v1.x += v2.x;
	v1.y += v2.y;
	v1.z += v2.z;
	return (v1);
}
