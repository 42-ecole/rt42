/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat_op.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:53:48 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:53:49 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_4x4matrix	matrix_mul(t_4x4matrix matrix1, t_4x4matrix matrix2)
{
	t_4x4matrix res;
	unsigned	i;
	unsigned	j;

	i = -1;
	while (++i < 4 && (j = -1))
		while (++j < 4)
			res.value[i][j] = matrix1.value[i][0] * matrix2.value[0][j] +
								matrix1.value[i][1] * matrix2.value[1][j] +
								matrix1.value[i][2] * matrix2.value[2][j] +
								matrix1.value[i][3] * matrix2.value[3][j];
	return (res);
}

t_4x4matrix	matrix_sum(t_4x4matrix matrix1, t_4x4matrix matrix2)
{
	unsigned	i;
	unsigned	j;

	i = -1;
	while (++i < 4 && (j = -1))
		while (++j < 4)
			matrix1.value[i][j] += matrix2.value[i][j];
	return (matrix1);
}

t_4x4matrix	matrix_scalar_mul(t_4x4matrix matrix, double number)
{
	unsigned	i;
	unsigned	j;

	i = -1;
	while (++i < 4 && (j = -1))
		while (++j < 4)
			matrix.value[i][j] *= number;
	return (matrix);
}
