/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_plane.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:54:11 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:54:12 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

double		plane_solve_x(t_plane_equation plane, double y, double z)
{
	return (-(plane.b * y + plane.c * z + plane.d) / plane.a);
}

double		plane_solve_y(t_plane_equation plane, double x, double z)
{
	return (-(plane.a * x + plane.c * z + plane.d) / plane.b);
}

double		plane_solve_z(t_plane_equation plane, double x, double y)
{
	return (-(plane.a * x + plane.b * y + plane.d) / plane.c);
}
