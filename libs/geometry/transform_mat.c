/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transform_mat.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:54:23 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:54:23 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_4x4matrix		get_translation_mat(t_vector vector)
{
	t_4x4matrix	res;

	res = identity_mat();
	res.value[3][0] = vector.x;
	res.value[3][1] = vector.y;
	res.value[3][2] = vector.z;
	return (res);
}

t_4x4matrix		get_scale_mat(double x, double y, double z)
{
	t_4x4matrix	res;

	res = identity_mat();
	res.value[0][0] = x;
	res.value[1][1] = y;
	res.value[2][2] = z;
	return (res);
}
