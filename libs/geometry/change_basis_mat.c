/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   change_basis_mat.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:53:23 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:53:25 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_4x4matrix	change_basis_mat(t_vector y, t_vector z, t_vector position)
{
	t_4x4matrix translation;
	t_4x4matrix	rotation;
	t_vector	x;

	translation = get_translation_mat(mul_vec3(position, -1.));
	rotation = identity_mat();
	x = normal_vec3(cross(y, z));
	rotation.value[0][0] = x.x;
	rotation.value[0][1] = x.y;
	rotation.value[0][2] = x.z;
	rotation.value[1][0] = y.x;
	rotation.value[1][1] = y.y;
	rotation.value[1][2] = y.z;
	rotation.value[2][0] = z.x;
	rotation.value[2][1] = z.y;
	rotation.value[2][2] = z.z;
	return (matrix_mul(translation, rotation));
}
