/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_mat_mult.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:54:35 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:54:36 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_vector	vec3_mul_mat(t_vector vector,
	t_4x4matrix matrix)
{
	t_vector res;

	res.x = matrix.value[0][0] * vector.x +
			matrix.value[1][0] * vector.y +
			matrix.value[2][0] * vector.z +
			matrix.value[3][0] * vector.w;
	res.y = matrix.value[0][1] * vector.x +
			matrix.value[1][1] * vector.y +
			matrix.value[2][1] * vector.z +
			matrix.value[3][1] * vector.w;
	res.z = matrix.value[0][2] * vector.x +
			matrix.value[1][2] * vector.y +
			matrix.value[2][2] * vector.z +
			matrix.value[3][2] * vector.w;
	res.w = matrix.value[0][3] * vector.x +
			matrix.value[1][3] * vector.y +
			matrix.value[2][3] * vector.z +
			matrix.value[3][3] * vector.w;
	return (res);
}

t_vector	mat_mul_vec3(t_4x4matrix matrix,
	t_vector vector)
{
	t_vector res;

	res.x = matrix.value[0][0] * vector.x +
			matrix.value[0][1] * vector.y +
			matrix.value[0][2] * vector.z +
			matrix.value[0][3] * vector.w;
	res.y = matrix.value[1][0] * vector.x +
			matrix.value[1][1] * vector.y +
			matrix.value[1][2] * vector.z +
			matrix.value[1][3] * vector.w;
	res.z = matrix.value[2][0] * vector.x +
			matrix.value[2][1] * vector.y +
			matrix.value[2][2] * vector.z +
			matrix.value[2][3] * vector.w;
	res.w = matrix.value[3][0] * vector.x +
			matrix.value[3][1] * vector.y +
			matrix.value[3][2] * vector.z +
			matrix.value[3][3] * vector.w;
	return (res);
}
