/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve_quadratic_equation.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:54:16 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:54:17 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

int	solve_quadratic_equation(t_quadratic_equation equation, double *t0,
	double *t1)
{
	double	discr;
	double	r0;
	double	r1;

	discr = equation.b * equation.b - 4 * equation.a * equation.c;
	if (discr < 0)
		return (0);
	discr = sqrt(discr);
	r0 = (-equation.b - discr) / (2. * equation.a);
	r1 = (-equation.b + discr) / (2. * equation.a);
	*t0 = (r0 < r1) ? r0 : r1;
	*t1 = (r0 < r1) ? r1 : r0;
	return (1);
}
