/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_view_mat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:54:04 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:54:04 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_4x4matrix	ray_view_mat(t_ray ray)
{
	t_4x4matrix	translation;
	t_4x4matrix	rotation;
	t_vector	view_axis;

	view_axis = (t_vector){0., 0., -1., 0.};
	ray.direction = normal_vec3(ray.direction);
	translation = get_translation_mat(mul_vec3(ray.origin, -1.));
	rotation = get_rotation_matrix_arbitrary(
					normal_vec3(cross(ray.direction, view_axis)),
					-angles_vecs3(ray.direction, view_axis));
	return (matrix_mul(translation, rotation));
}
