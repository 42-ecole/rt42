/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3_op2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:54:44 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:54:45 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "geometry.h"

t_vector	sub_vec3(t_vector v1, t_vector v2)
{
	v1.x -= v2.x;
	v1.y -= v2.y;
	v1.z -= v2.z;
	v1.w -= v2.w;
	return (v1);
}

double		angles_vecs3(t_vector v1, t_vector v2)
{
	v1 = normal_vec3(v1);
	v2 = normal_vec3(v2);
	return (acos(dot(v1, v2)));
}
