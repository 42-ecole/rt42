/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   netpbm_helpers.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:12:58 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:12:58 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "netpbm_internal.h"

t_netpbm_image	form_image(size_t width, size_t height)
{
	t_netpbm_image	res;

	res.image_data = ft_memalloc(sizeof(int) * width * height);
	res.width = width;
	res.height = height;
	return (res);
}

t_netpbm_image	new_image(void)
{
	return ((t_netpbm_image){
			.image_data = NULL,
			.width = 0,
			.height = 0});
}
