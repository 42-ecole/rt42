/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_ppm_ascii.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:13:23 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:13:24 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "netpbm_internal.h"

t_netpbm_image	read_ppm_ascii(int fd, int *status)
{
	t_netpbm_image	res;

	(void)fd;
	res.image_data = NULL;
	*status = FILE_ERROR;
	return (res);
}
