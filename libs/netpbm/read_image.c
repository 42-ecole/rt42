/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_image.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:13:14 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:13:15 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "netpbm_internal.h"

t_netpbm_image	read_image(const char *file, int *status)
{
	t_netpbm_image	res;
	char			buf[2];
	int				fd;

	res.image_data = NULL;
	if ((fd = open(file, O_RDONLY)) == -1)
	{
		*status = FILE_ERROR;
		return (res);
	}
	read(fd, buf, 2);
	if (ft_strnequ(buf, "P3", 2))
		res = read_ppm_ascii(fd, status);
	else if (ft_strnequ(buf, "P6", 2))
		res = read_ppm_binary(fd, status);
	else
		*status = INVALID_FORMAT;
	close(fd);
	return (res);
}
