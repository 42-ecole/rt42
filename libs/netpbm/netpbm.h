/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   netpbm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:13:07 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:13:08 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NETPBM_H
# define NETPBM_H
# define FILE_ERROR -1
# define INVALID_FORMAT -2

# include <stdlib.h>

typedef struct	s_netpbm_image
{
	int		*image_data;
	size_t	width;
	size_t	height;
}				t_netpbm_image;

t_netpbm_image	read_image(const char *file, int *status);

#endif
