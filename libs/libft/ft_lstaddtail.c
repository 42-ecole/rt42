/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstaddtail.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:18:48 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:18:49 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstaddtail(t_list **begin, t_list *new)
{
	t_list	*tmp;

	if (begin)
	{
		tmp = *begin;
		if (!tmp)
		{
			*begin = new;
			return ;
		}
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = new;
	}
}
