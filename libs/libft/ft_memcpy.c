/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:22:39 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:22:39 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t	i;
	size_t	j;

	i = -1;
	while (++i < n / sizeof(unsigned long) && dst != src)
		((unsigned long *)dst)[i] = ((unsigned long *)src)[i];
	j = i * sizeof(unsigned long);
	i = -1;
	while (++i + j < n && dst != src)
		((unsigned char *)dst)[i + j] = ((unsigned char *)src)[i + j];
	return (dst);
}
