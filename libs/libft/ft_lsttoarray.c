/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsttoarray.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:19:26 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:19:27 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_lsttoarray(t_list *list)
{
	void	*res;
	size_t	size;
	t_list	*lst;

	res = NULL;
	size = 1;
	lst = list;
	while (lst && (size += lst->content_size))
		lst = lst->next;
	lst = list;
	if (--size)
	{
		res = ft_memalloc(size);
		size = 0;
		while (lst)
		{
			ft_memcpy(res + size, lst->content, lst->content_size);
			size += lst->content_size;
			lst = lst->next;
		}
	}
	return (res);
}
