/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:24:48 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:24:49 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t i;
	size_t j;

	i = ft_strlen(dst);
	j = ft_strlen(src);
	if (i < size)
	{
		if (j + i < size)
			ft_memcpy(dst + i, src, j + 1);
		else
		{
			ft_memcpy(dst + i, src, size - i - 1);
			dst[size - 1] = '\0';
		}
	}
	return ((size < i ? size : i) + j);
}
