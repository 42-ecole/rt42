/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:26:18 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:26:18 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	size_t i;
	size_t nlen;

	i = -1;
	nlen = ft_strlen(needle);
	while (haystack[++i] && *needle)
		if (ft_memcmp(haystack + i, needle, nlen) == 0)
			return ((char *)haystack + i);
	return ((*needle) ? NULL : (char *)haystack);
}
