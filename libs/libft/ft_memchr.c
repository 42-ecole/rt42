/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:22:28 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:22:29 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char	*arr;
	unsigned char		sym;
	size_t				i;

	i = -1;
	arr = s;
	sym = c;
	while (++i < n)
		if (arr[i] == sym)
			return ((void *)arr + i);
	return (NULL);
}
