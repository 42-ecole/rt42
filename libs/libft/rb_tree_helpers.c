/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rb_tree_helpers.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:27:11 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:27:11 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rb_tree.h"

t_rb_tree	*rb_grandparent(t_rb_tree *node)
{
	if (node && node->parent)
		return (node->parent->parent);
	return (NULL);
}

t_rb_tree	*rb_uncle(t_rb_tree *node)
{
	t_rb_tree	*grandparent;

	if ((grandparent = rb_grandparent(node)))
	{
		if (grandparent->left == node)
			return (grandparent->right);
		else
			return (grandparent->left);
	}
	return (NULL);
}
