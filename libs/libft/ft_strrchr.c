/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:26:06 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:26:07 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	size_t i;

	i = -1;
	while (s[++i])
		;
	if ((char)c == '\0')
		return ((char *)s + i);
	while (i > 0)
		if (s[i - 1] == (char)c)
			return ((char *)s + i - 1);
		else
			--i;
	return (NULL);
}
