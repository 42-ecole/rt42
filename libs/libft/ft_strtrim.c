/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:19:34 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:19:36 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtrim(char const *s)
{
	char	*res;
	size_t	i;
	char	*limit[2];
	size_t	len;

	i = -1;
	limit[0] = NULL;
	limit[1] = NULL;
	res = NULL;
	if (s)
	{
		len = ft_strlen(s) - 1;
		while (s[++i] && (limit[0] == NULL || limit[1] == NULL))
		{
			if (!ft_isspace(s[i]) && limit[0] == NULL)
				limit[0] = (char *)s + i;
			if (!ft_isspace(s[len - i]) && limit[1] == NULL)
				limit[1] = (char *)s + len - i + 1;
		}
		res = ft_strsub(s, (unsigned)(limit[0] - s), limit[1] - limit[0]);
	}
	return (res);
}
