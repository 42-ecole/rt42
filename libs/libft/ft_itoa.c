/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:21:19 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:21:20 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	size_t	size;
	size_t	i;
	long	numb;
	char	*res;

	numb = n;
	i = 0;
	size = n < 0 ? 2 : 1;
	while (numb / 10 && ++size)
		numb = numb / 10;
	res = ft_strnew(size);
	if (n < 0 && res)
		res[0] = '-';
	numb = n < 0 ? -(long)n : (long)n;
	while ((numb || i == 0) && res)
	{
		res[size - 1 - i++] = (numb % 10) + '0';
		numb = numb / 10;
	}
	return (res);
}
