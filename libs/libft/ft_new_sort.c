/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_new_sort.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:22:57 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:22:57 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_sort_data		ft_new_sort(int (*compare)(const void *e1, const void *e2,
	size_t size), void (*swap)(void *e1, void *e2, size_t size), size_t size)
{
	t_sort_data res;

	res.compare = compare;
	res.swap = swap;
	res.size = size;
	return (res);
}
