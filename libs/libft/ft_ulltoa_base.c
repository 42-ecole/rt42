/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ulltoa_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:26:41 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:26:41 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_ulltoa_base(unsigned long long n, const char *base, unsigned nbase)
{
	size_t				size;
	size_t				i;
	unsigned long long	numb;
	char				*res;

	res = NULL;
	if (nbase >= 2 && nbase <= 16)
	{
		numb = n;
		size = 1;
		while (numb / nbase && ++size)
			numb = numb / nbase;
		res = ft_strnew(size);
		numb = n;
		i = 0;
		while ((numb || i == 0) && res)
		{
			res[size - 1 - (i++)] =
				(base ? base : "0123456789ABCDEF")[ABS(numb % nbase)];
			numb = numb / nbase;
		}
	}
	return (res);
}
