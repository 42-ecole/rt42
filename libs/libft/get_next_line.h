/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:27:06 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:27:07 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUFF_SIZE 48
# include "libft.h"

typedef struct	s_gnlfd
{
	int		fd;
	char	*buf;
}				t_gnlfd;

int				get_next_line(const int fd, char **line);

#endif
