/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:24:00 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:24:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	size_t i;

	i = -1;
	if (s[0] == (char)c)
		return ((char *)s);
	while (s[++i])
		if (s[i + 1] == (char)c)
			return ((char *)s + i + 1);
	return (NULL);
}
