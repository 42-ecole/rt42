/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:25:33 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:25:34 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned	i;
	char		*res;

	i = -1;
	res = NULL;
	if (s)
	{
		res = (char *)ft_strnew(ft_strlen(s));
		while (s[++i] && res)
			res[i] = f(i, s[i]);
	}
	return (res);
}
