/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rb_tree.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:27:15 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:27:16 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RB_TREE_H
# define RB_TREE_H

# include <stdlib.h>

typedef struct	s_rb_tree
{
	struct s_rb_tree	*left;
	struct s_rb_tree	*right;
	struct s_rb_tree	*parent;
	void				*content;
	size_t				content_size;
	char				black;
}				t_rb_tree;

t_rb_tree		*grandparent(t_rb_tree *node);

#endif
