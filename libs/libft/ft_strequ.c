/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:24:29 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:24:30 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strequ(char const *s1, char const *s2)
{
	size_t i;

	i = -1;
	while (s1 && s2 && s1 != s2 && s1[++i])
		if (s1[i] != s2[i])
			return (0);
	return ((s1 == s2) || (s1 && s2 && s1[i] == s2[i]));
}
