/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:26:02 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:26:02 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	char *res;

	res = ft_strstr(haystack, needle);
	res = res + ft_strlen(needle) > haystack + len ? NULL : res;
	return (*needle ? res : (char *)haystack);
}
