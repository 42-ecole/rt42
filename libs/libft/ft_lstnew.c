/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:22:09 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:22:10 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*res;
	void	*cont;

	res = (t_list *)ft_memalloc(sizeof(t_list));
	if (res && content && content_size)
	{
		if ((cont = ft_memalloc(content_size)))
		{
			ft_memcpy(cont, content, content_size);
			res->content = cont;
			res->content_size = content_size;
		}
		else
			ft_memdel((void **)&res);
	}
	return (res);
}
