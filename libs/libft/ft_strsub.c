/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:26:22 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:26:22 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	size_t	sindex;
	char	*res;

	sindex = (size_t)start;
	i = -1;
	res = ft_strnew(len);
	if (res && s)
		ft_memcpy(res, s + sindex, len);
	return (res);
}
