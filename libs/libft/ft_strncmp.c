/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:25:42 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:25:42 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t len)
{
	size_t i;

	i = 0;
	if (len == 0)
		return (0);
	while (i < len && s1[i] && s1[i] == s2[i] && s1 != s2)
		++i;
	return (((unsigned char)s1[i == len ? len - 1 : i] -
			(unsigned char)s2[i == len ? len - 1 : i]));
}
