/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/24 15:22:48 by rfunk             #+#    #+#             */
/*   Updated: 2020/10/24 15:22:48 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	unsigned char		*d;
	const unsigned char	*s;

	if (src > dst)
		ft_memcpy(dst, src, len);
	else
	{
		++len;
		d = dst;
		s = src;
		while (--len)
			d[len - 1] = s[len - 1];
	}
	return (dst);
}
