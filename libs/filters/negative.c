/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   negative.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:16:19 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:16:20 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filters.h"

t_vector	negative_scale(t_vector *colors, size_t size)
{
	t_vector	res;

	(void)size;
	res.x = 1. - colors[0].x;
	res.y = 1. - colors[0].y;
	res.z = 1. - colors[0].z;
	return (res);
}
