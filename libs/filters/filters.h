/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filters.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:15:57 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:15:58 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILTERS_H
# define FILTERS_H

# define SIZE 6

# include "rt.h"

t_vector		negative_scale(t_vector *colors, size_t size);
t_vector		sepia_scale(t_vector *colors, size_t size);
t_vector		gray_scale(t_vector *colors, size_t size);
t_vector		blur(t_vector *colors, size_t size);
t_vector		median(t_vector *colors, size_t size);
t_vector		get_color_from_pixel(t_pixel pixel);

#endif
