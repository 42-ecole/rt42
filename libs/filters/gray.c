/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gray.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:16:06 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:16:06 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filters.h"

t_vector	gray_scale(t_vector *colors, size_t size)
{
	double		color;
	t_vector	res;

	(void)size;
	color = (colors[0].x + colors[0].y + colors[0].z) / 3;
	res.x = color;
	res.y = color;
	res.z = color;
	return (res);
}
