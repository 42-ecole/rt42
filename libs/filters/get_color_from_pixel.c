/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color_from_pixel.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:16:02 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:16:02 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filters.h"

t_vector	get_color_from_pixel(t_pixel pixel)
{
	if (pixel.divider == 0)
		return ((t_vector){0, 0, 0, 0});
	if (pixel.divider == 1)
		return (pixel.color);
	return (mul_vec3(pixel.color, 1. / (double)pixel.divider));
}
