/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   median.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:16:15 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:16:16 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filters.h"

int			compare(const void *e1, const void *e2, size_t size)
{
	t_vector	v1;
	t_vector	v2;

	v1 = *((t_vector*)e1);
	v2 = *((t_vector*)e2);
	(void)size;
	if ((v1.x + v1.y + v1.z) - (v2.x + v2.y + v2.z) < 0)
		return (-1);
	return (0);
}

t_vector	median(t_vector *colors, size_t size)
{
	t_sort_data	data;

	data = ft_new_sort(compare, NULL, sizeof(t_vector));
	ft_uqsort((void*)colors, size * size, data);
	return (colors[size * size / 2]);
}
