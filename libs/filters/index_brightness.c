/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index_brightness.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcremin <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:16:10 by jcremin           #+#    #+#             */
/*   Updated: 2020/11/01 15:16:11 by jcremin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filters.h"

double	get_index_brightness(double scale)
{
	static double	brightness = 1.;

	brightness *= scale;
	return (brightness);
}
