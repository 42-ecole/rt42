/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   receive_task.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:57:09 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:57:09 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "network_api.h"

static t_task	*current_task(void)
{
	static t_task	task;

	return (&task);
}

void			receive_task(dyad_Event *event)
{
	t_task		*task;
	t_render	*render;

	task = current_task();
	render = ((t_connection*)event->udata)->render;
	ft_memcpy((void*)task, (void*)event->data, sizeof(t_task));
	render->camera = task->meta.camera;
	render->window_width = task->meta.width;
	render->window_height = task->meta.height;
	render->working = 1;
	render->task = task;
}
