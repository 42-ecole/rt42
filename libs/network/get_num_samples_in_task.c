/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_num_samples_in_task.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:56:02 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:56:03 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "network_api.h"

size_t	get_num_samples_in_task(void)
{
	return ((8192 - sizeof(t_task_meta)) / sizeof(t_sample) - 1);
}
