/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_singleton.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:55:54 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:55:54 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "network_api.h"

t_connection	*get_connection(void)
{
	static t_connection	connection;

	return (&connection);
}

t_hash			*map_dependent_hash(void)
{
	static t_hash	hash[2];

	return (hash);
}
