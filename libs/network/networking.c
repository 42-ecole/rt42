/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   networking.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:56:26 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:56:26 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "network_api.h"

int		its_time(uint32_t delay)
{
	static uint32_t	last_request;
	uint32_t		current_time;

	current_time = SDL_GetTicks() - last_request;
	if (SDL_TICKS_PASSED(current_time, delay))
	{
		last_request = SDL_GetTicks();
		return (1);
	}
	return (0);
}

void	networking(t_render *render)
{
	dyad_update();
	if (render->mode != WORKER)
		return ;
	else if (!render->working && its_time(100))
		request_task(render->stream);
}
