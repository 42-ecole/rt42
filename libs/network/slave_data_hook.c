/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slave_data_hook.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:57:25 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:57:25 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "network_api.h"

void	slave_data_hook(dyad_Event *event)
{
	if (event->size == sizeof(t_task) && check_validity_task(event) == 0)
		receive_task(event);
}
