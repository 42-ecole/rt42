/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_hook.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:55:48 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:55:49 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "network_api.h"

void	data_hook(dyad_Event *event)
{
	if (!(event && event->data))
		return ;
	if (((t_connection*)event->udata)->render->mode == MASTER)
		master_data_hook(event);
	else if (((t_connection*)event->udata)->render->mode == WORKER)
		slave_data_hook(event);
}
