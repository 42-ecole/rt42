/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 17:56:06 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 17:56:06 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "network_api.h"

void	on_connect(dyad_Event *e)
{
	(void)e;
	return ;
}

void	release_connection(dyad_Event *e)
{
	free(e->udata);
}

void	on_closed(dyad_Event *e)
{
	(void)e;
	exit(0);
}

void	on_error(dyad_Event *event)
{
	(void)event;
}
