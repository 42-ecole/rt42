/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 15:07:40 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 15:07:41 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_write_wrapper.h"
#include <fcntl.h>

char	*read_map(char *filename)
{
	t_wwrapper	wrap;
	char		buf[128];
	char		*res;
	int			fd;
	int			rd;

	res = NULL;
	wrap.type = 2;
	wrap.content.str_ptr = &res;
	wrap.size = 0;
	wrap.bytes_written = 0;
	if ((fd = open(filename, O_RDONLY)) > 0)
		while ((rd = read(fd, buf, 128)) > 0)
			ft_write_wrapper(&wrap, buf, (size_t)rd);
	ft_write_wrapper(&wrap, buf, 0);
	close(fd);
	return (res);
}
