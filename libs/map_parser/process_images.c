/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_images.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:41:14 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:41:15 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map_parse.h"

int		add_image(t_netpbm_image *image, t_json json, char *location)
{
	int		status;
	char	*name_file;
	char	*tmp;

	if (json.type != JSON_STRING)
		return (-1);
	name_file = ft_strjoin(location, "/resources/images/");
	tmp = name_file;
	name_file = ft_strjoin(name_file, json.value.string);
	free(tmp);
	*image = read_image(name_file, &status);
	free(name_file);
	if (status)
		return (-1);
	return (0);
}

int		process_images(t_render *render, t_json json, int load_images)
{
	size_t	i;

	if (json.type != JSON_ARRAY && json.type != JSON_NULL)
		return (-1);
	render->n_images = json.size;
	render->images = NULL;
	if (json.type == JSON_NULL)
		return (0);
	if (!(render->images =
			(t_netpbm_image*)ft_memalloc(sizeof(t_netpbm_image) * json.size)))
		return (-1);
	i = -1;
	if (load_images)
		while (++i < json.size)
			if (add_image(&render->images[i], json_value_by_index(json, i),
					render->location))
				return (-1);
	return (0);
}
