/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_map.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:41:25 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:41:25 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map_parse.h"

int	process_map(t_render *render, t_json json, int load_images)
{
	if (process_camera(render, json_value_by_key(json, "camera")))
		return (-1);
	if (process_images(render, json_value_by_key(json, "images"), load_images))
		return (-1);
	if (process_materials(render, json_value_by_key(json, "materials")))
		return (-1);
	if (process_lights(render, json_value_by_key(json, "lights")))
		return (-1);
	if (process_objects(render, json_value_by_key(json, "objects")))
		return (-1);
	if (process_transformations(render,
			json_value_by_key(json, "transformations")))
		return (-1);
	return (0);
}
