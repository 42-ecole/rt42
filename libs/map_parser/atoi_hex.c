/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi_hex.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:40:08 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:40:09 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	atoi_hex(char *str)
{
	int		res;
	char	*base;

	res = 0;
	base = "0123456789abcdef";
	while (ft_isspace(*str))
		str++;
	if (str[0] != '0' || ft_tolower(str[1]) != 'x')
		return (0);
	str += 2;
	while (*str)
	{
		res = res * 16 + (ft_strchr(base, ft_tolower(*str)) - base);
		str++;
	}
	return (res);
}
