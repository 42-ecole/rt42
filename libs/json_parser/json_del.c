/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   json_del.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:38:10 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:38:11 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "json_internal.h"

void		json_del(void *content, size_t size)
{
	t_json	*json;

	(void)size;
	json = (t_json*)content;
	json_destroy(json);
	free(json);
}
