/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   element_lack.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:37:55 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:37:57 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "json_internal.h"

int	element_lack(char **json_string, int *status, char end)
{
	if (**json_string == ',')
	{
		*json_string += 1;
		skip_spaces(json_string);
		if (**json_string == end)
		{
			*status = -1;
			return (1);
		}
	}
	return (0);
}
