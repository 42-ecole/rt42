/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   json_internal.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:38:27 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:38:28 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef JSON_INTERNAL_H
# define JSON_INTERNAL_H

# include "json.h"
# include "libft.h"
# include <math.h>

void	json_del(void *content, size_t size);
int		element_lack(char **json_string, int *status, char end);
void	skip_spaces(char **json_string);
char	*get_string(char **json_string);

t_json	parse_string(char **json_string, int *status);
t_json	parse_number(char **json_string, int *status);
t_json	parse_bool(char **json_string, int *status);
t_json	parse_null(char **json_string, int *status);

t_json	parse_element(char **json_string, int *status);
t_json	parse_array(char **json_string, int *status);
t_json	parse_object(char **json_string, int *status);

#endif
