/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_number.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:39:36 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:39:37 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "json_internal.h"

static void	iterate_str(char **json_string)
{
	char	*str;

	str = *json_string;
	if (*str == '-' || *str == '+')
		++str;
	while (ft_isdigit(*str))
		++str;
	if (*str == '.')
	{
		++str;
		while (ft_isdigit(*str))
			++str;
	}
	if (*str == 'e' || *str == 'E')
	{
		++str;
		if (*str == '-' || *str == '+')
			++str;
		while (ft_isdigit(*str))
			++str;
	}
	*json_string = str;
}

t_json		parse_number(char **json_string, int *status)
{
	t_json	res;

	*status = 0;
	res = new_json();
	res.type = JSON_NUMBER;
	res.value.number = ft_atof(*json_string);
	if (isnan(res.value.number))
		*status = -1;
	iterate_str(json_string);
	return (res);
}
