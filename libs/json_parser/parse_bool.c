/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_bool.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:39:17 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:39:17 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "json_internal.h"

static int	ft_get_boolean(char **str)
{
	if (ft_strnequ("true", *str, 4))
	{
		*str += 4;
		return (1);
	}
	if (ft_strnequ("false", *str, 5))
	{
		*str += 5;
		return (0);
	}
	return (-1);
}

t_json		parse_bool(char **json_string, int *status)
{
	t_json	res;

	*status = 0;
	res = new_json();
	res.type = JSON_BOOL;
	res.value.boolean = ft_get_boolean(json_string);
	if (res.value.boolean == -1)
		*status = -1;
	return (res);
}
