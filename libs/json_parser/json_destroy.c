/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   json_destroy.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:38:18 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:38:19 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "json_internal.h"

void	json_destroy(t_json *json)
{
	size_t	i;

	if (json)
	{
		ft_memdel((void*)&json->key);
		if (json->type == JSON_STRING)
			free(json->value.string);
		else if (json->type == JSON_ARRAY || json->type == JSON_OBJECT)
		{
			i = -1;
			while (++i < json->size)
				json_destroy((json->type == JSON_ARRAY) ?
						&json->value.array[i] : &json->value.object[i]);
			free((json->type == JSON_ARRAY) ?
					json->value.array : json->value.object);
		}
		json->type = JSON_NULL;
	}
}
