/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   json_value_by_index.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:38:43 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:38:43 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "json.h"

t_json	json_value_by_index(t_json json, size_t index)
{
	if (json.type == JSON_ARRAY && index < json.size)
		return (json.value.array[index]);
	return (new_json());
}
