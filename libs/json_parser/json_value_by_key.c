/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   json_value_by_key.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:38:48 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:38:49 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "json_internal.h"

t_json	json_value_by_key(t_json json, char *key)
{
	size_t	i;

	i = -1;
	if (json.type == JSON_OBJECT && key)
		while (++i < json.size)
			if (ft_strequ(json.value.object[i].key, key))
				return (json.value.object[i]);
	return (new_json());
}
