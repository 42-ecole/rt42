/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   json_parse.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: raskar <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:38:36 by raskar            #+#    #+#             */
/*   Updated: 2020/11/01 14:38:36 by raskar           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "json_internal.h"

t_json	json_parse(char *json_string)
{
	t_json	res;
	char	*tmp;
	int		status;

	res = new_json();
	if (json_string)
	{
		json_string = ft_strtrim(json_string);
		if ((tmp = json_string))
		{
			res = parse_element(&tmp, &status);
			if (*tmp || status != 0)
				json_destroy(&res);
		}
		free(json_string);
	}
	return (res);
}
