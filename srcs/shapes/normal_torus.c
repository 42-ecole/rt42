/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal_torus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:26 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:26 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	normal_torus(t_intersection *inter, t_ray ray)
{
	t_4x4matrix	new_basis;
	t_vector	y_axis;
	t_vector	z_axis;
	t_vector	color;
	t_vector	point;

	point = sum_vec3(ray.origin, mul_vec3(ray.direction, inter->z));
	point = sub_vec3(point, ((t_torus*)inter->primitive->primitive)->center);
	point = vec3_mul_mat(point,
			((t_torus*)inter->primitive->primitive)->matrix);
	point.z = 0.;
	point.w = 0.;
	y_axis = vec3_mul_mat((t_vector){0, 0, 1, 0},
			((t_torus*)inter->primitive->primitive)->matrix);
	if (norm(point) < ((t_torus*)inter->primitive->primitive)->rt)
		y_axis = mul_vec3(y_axis, -1);
	z_axis = normal_vec3(inter->normal);
	y_axis = normal_vec3(cross(cross(y_axis, z_axis), z_axis));
	new_basis = change_basis_mat(y_axis, z_axis, (t_vector){0, 0, 0, 1});
	color = get_normal_uv(inter->primitive, inter->uv);
	inter->mapped_normal = normal_vec3(vec3_mul_mat(color, new_basis));
}
