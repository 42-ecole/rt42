/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cone.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:49:42 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:49:42 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_cone	new_cone(double height, double angle)
{
	return ((t_cone){
			.tip = (t_vector){0, 0, 0, 1},
			.direction = (t_vector){0, 0, -1, 0},
			.radius_vector = (t_vector){-1, 0, 0, 0},
			.height = height,
			.angle = angle});
}

t_cone	move_cone(t_cone cone, t_vector vector)
{
	cone.tip = sum_vec3(cone.tip, vector);
	return (cone);
}

t_cone	rotate_cone_os(t_cone cone, double x_angle, double y_angle,
	double z_angle)
{
	t_4x4matrix	rotation;
	t_vector	translation;

	translation = sub_vec3(cone.tip,
			(t_vector){0., 0., cone.height / 2., 1.});
	rotation = matrix_mul(
					matrix_mul(
						get_rotation_matrix_arbitrary(
							normal_vec3(
								cross(
									cone.radius_vector,
									cone.direction)),
							y_angle),
						get_rotation_matrix_arbitrary(cone.radius_vector,
							x_angle)),
					get_rotation_matrix_arbitrary(cone.direction,
						-z_angle));
	cone.tip = sub_vec3(cone.tip, translation);
	cone.tip = vec3_mul_mat(cone.tip, rotation);
	cone.tip = sum_vec3(cone.tip, translation);
	cone.direction = vec3_mul_mat(cone.direction, rotation);
	cone.radius_vector = vec3_mul_mat(cone.radius_vector, rotation);
	return (cone);
}

t_cone	transform_cone_ws(t_cone cone, t_4x4matrix matrix)
{
	cone.tip = vec3_mul_mat(cone.tip, matrix);
	cone.direction = normal_vec3(vec3_mul_mat(cone.direction, matrix));
	cone.radius_vector = normal_vec3(
		vec3_mul_mat(cone.radius_vector, matrix));
	return (cone);
}
