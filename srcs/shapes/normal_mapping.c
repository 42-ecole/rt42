/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal_mapping.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:08 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:08 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_vector	get_normal_uv(t_primitive *primitive, t_uv uv)
{
	t_vector	res;

	res = color2vec(get_color_uv(primitive->normal_map, uv));
	res.x -= 0.5;
	res.y -= 0.5;
	res.z -= 0.5;
	return (normal_vec3(res));
}

void		map_normal(t_intersection *inter, t_ray ray)
{
	if (inter->primitive->normal_map.image)
	{
		if (inter->primitive->type == TRIANGLE)
			normal_triangle(inter);
		else if (inter->primitive->type == PLANE)
			normal_plane(inter);
		else if (inter->primitive->type == SPHERE)
			normal_sphere(inter);
		if (inter->primitive->type == CYLINDER)
			normal_cylinder(inter);
		if (inter->primitive->type == CIRCLE)
			normal_circle(inter);
		if (inter->primitive->type == CONE)
			normal_cone(inter);
		if (inter->primitive->type == TORUS)
			normal_torus(inter, ray);
		if (inter->primitive->type == PARABOLOID)
			normal_paraboloid(inter);
	}
	else
		inter->mapped_normal = inter->normal;
}
