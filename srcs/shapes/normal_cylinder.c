/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal_cylinder.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:03 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:03 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	normal_cylinder(t_intersection *inter)
{
	t_4x4matrix	new_basis;
	t_vector	y_axis;
	t_vector	z_axis;

	z_axis = inter->normal;
	y_axis = cross(
		((t_cylinder*)inter->primitive->primitive)->direction,
		z_axis);
	new_basis = change_basis_mat(y_axis, z_axis, (t_vector){0, 0, 0, 0});
	inter->mapped_normal = vec3_mul_mat(
			get_normal_uv(inter->primitive, inter->uv), new_basis);
}
