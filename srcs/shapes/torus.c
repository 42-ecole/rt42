/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   torus.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:48 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:48 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_torus	new_torus(double rt, double rc)
{
	t_4x4matrix	m;

	m = identity_mat();
	return ((t_torus){
			.center = (t_vector){0, 0, 0, 1},
			.matrix = m,
			.rt = rt,
			.rc = rc});
}

t_torus	move_torus(t_torus torus, t_vector vector)
{
	torus.center = sum_vec3(torus.center, vector);
	return (torus);
}

t_torus	transform_torus_ws(t_torus torus, t_4x4matrix matrix)
{
	torus.center = vec3_mul_mat(torus.center, matrix);
	torus.matrix = invert_mat(matrix_mul(
				invert_mat(torus.matrix), matrix));
	return (torus);
}
