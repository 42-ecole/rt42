/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal_cone.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:49:58 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:49:59 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	normal_cone(t_intersection *inter)
{
	t_4x4matrix	new_basis;
	t_vector	y_axis;
	t_vector	z_axis;

	z_axis = inter->normal;
	y_axis = cross(
		cross(
			mul_vec3(
				((t_cone*)inter->primitive->primitive)->direction, -1.),
			z_axis),
		z_axis);
	new_basis = change_basis_mat(y_axis, z_axis, (t_vector){0, 0, 0, 0});
	inter->mapped_normal = vec3_mul_mat(
			get_normal_uv(inter->primitive, inter->uv), new_basis);
}
