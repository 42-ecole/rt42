/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylinder.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:49:48 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:49:48 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_cylinder	new_cylinder(double height, double radius)
{
	return ((t_cylinder){
			.position = (t_vector){0., 0., -height / 2., 1.},
			.direction = (t_vector){0., 0., 1., 0.},
			.radius_vector = (t_vector){1., 0., 0., 0.},
			.height = height,
			.radius = radius});
}

t_cylinder	move_cylinder(t_cylinder cylinder, t_vector vector)
{
	cylinder.position = sum_vec3(cylinder.position, vector);
	return (cylinder);
}

t_cylinder	rotate_cylinder_os(t_cylinder cylinder, double x_angle,
	double y_angle, double z_angle)
{
	t_4x4matrix	rotation;
	t_vector	translation;

	translation = sub_vec3(cylinder.position,
			(t_vector){0., 0., -cylinder.height / 2., 1.});
	rotation = matrix_mul(
					matrix_mul(
						get_rotation_matrix_arbitrary(
							normal_vec3(
								cross(
									cylinder.direction,
									cylinder.radius_vector)),
							y_angle),
						get_rotation_matrix_arbitrary(cylinder.radius_vector,
							x_angle)),
					get_rotation_matrix_arbitrary(cylinder.direction,
						z_angle));
	cylinder.position = sub_vec3(cylinder.position, translation);
	cylinder.position = vec3_mul_mat(cylinder.position, rotation);
	cylinder.position = sum_vec3(cylinder.position, translation);
	cylinder.direction = vec3_mul_mat(cylinder.direction, rotation);
	cylinder.radius_vector = vec3_mul_mat(
			cylinder.radius_vector, rotation);
	return (cylinder);
}

t_cylinder	transform_cylinder_ws(t_cylinder cylinder, t_4x4matrix matrix)
{
	cylinder.position = vec3_mul_mat(cylinder.position, matrix);
	cylinder.direction = normal_vec3(
		vec3_mul_mat(cylinder.direction, matrix));
	cylinder.radius_vector = normal_vec3(
		vec3_mul_mat(cylinder.radius_vector, matrix));
	return (cylinder);
}
