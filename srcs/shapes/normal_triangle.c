/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal_triangle.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:30 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:30 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	normal_triangle(t_intersection *inter)
{
	t_triangle	*triangle;
	t_vector	tangent;
	t_4x4matrix	new_basis;
	t_vector	new_normal;

	triangle = (t_triangle*)inter->primitive->primitive;
	tangent = normal_vec3(sub_vec3(triangle->c, triangle->a));
	new_basis = change_basis_mat(
			tangent, inter->normal, (t_vector){0, 0, 0, 0});
	new_normal = get_normal_uv(inter->primitive, inter->uv);
	new_normal = normal_vec3(vec3_mul_mat(new_normal, new_basis));
	inter->mapped_normal = new_normal;
}
