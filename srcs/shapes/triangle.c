/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   triangle.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:53 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:53 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_triangle	new_triangle(double x_cathetus, double y_cathetus)
{
	t_triangle res;

	res.a = (t_vector){0, 0, 0, 1};
	res.b = (t_vector){x_cathetus, 0, 0, 1};
	res.c = (t_vector){0, y_cathetus, 0, 1};
	return (res);
}

t_triangle	move_triangle(t_triangle triangle, t_vector vector)
{
	triangle.a = sum_vec3(triangle.a, vector);
	triangle.b = sum_vec3(triangle.b, vector);
	triangle.c = sum_vec3(triangle.c, vector);
	return (triangle);
}

t_triangle	rotate_triangle_os(t_triangle triangle, double x_angle,
	double y_angle, double z_angle)
{
	t_4x4matrix	rotation;
	t_vector	translation;

	rotation = matrix_mul(
					matrix_mul(
						get_rotation_matrix_arbitrary(
							normal_vec3(cross(
								sub_vec3(triangle.b, triangle.a),
								sub_vec3(triangle.c, triangle.a))), z_angle),
						get_rotation_matrix_arbitrary(
							sub_vec3(triangle.c, triangle.a), y_angle)),
					get_rotation_matrix_arbitrary(
						sub_vec3(triangle.b, triangle.a), x_angle));
	translation = triangle.a;
	triangle.a = sub_vec3(triangle.a, translation);
	triangle.b = sub_vec3(triangle.c, translation);
	triangle.c = sub_vec3(triangle.b, translation);
	triangle.a = vec3_mul_mat(triangle.a, rotation);
	triangle.b = vec3_mul_mat(triangle.b, rotation);
	triangle.c = vec3_mul_mat(triangle.c, rotation);
	triangle.a = sum_vec3(triangle.a, translation);
	triangle.b = sum_vec3(triangle.c, translation);
	triangle.c = sum_vec3(triangle.b, translation);
	return (triangle);
}

t_triangle	transform_triangle_ws(t_triangle triangle, t_4x4matrix matrix)
{
	triangle.a = vec3_mul_mat(triangle.a, matrix);
	triangle.b = vec3_mul_mat(triangle.b, matrix);
	triangle.c = vec3_mul_mat(triangle.c, matrix);
	return (triangle);
}
