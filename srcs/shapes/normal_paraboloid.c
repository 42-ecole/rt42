/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normal_paraboloid.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:13 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:13 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	normal_paraboloid(t_intersection *inter)
{
	t_4x4matrix	new_basis;
	t_vector	y_axis;
	t_vector	z_axis;
	t_vector	color;

	z_axis = normal_vec3(inter->normal);
	y_axis = normal_vec3(cross(cross(mul_vec3(
				((t_paraboloid*)inter->primitive->primitive)->direction, -1),
				z_axis), z_axis));
	new_basis = change_basis_mat(y_axis, z_axis, (t_vector){0, 0, 0, 1});
	color = get_normal_uv(inter->primitive, inter->uv);
	inter->mapped_normal = normal_vec3(vec3_mul_mat(color, new_basis));
}
