/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   paraboloid.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:34 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:34 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_paraboloid	new_paraboloid(double height, double k)
{
	return ((t_paraboloid){
			.position = (t_vector){0, 0, 0, 1},
			.direction = (t_vector){0, 0, 1, 0},
			.vecr = (t_vector){-1, 0, 0, 0},
			.height = height,
			.k = k});
}

t_paraboloid	move_paraboloid(t_paraboloid paraboloid, t_vector vector)
{
	paraboloid.position = sum_vec3(paraboloid.position, vector);
	return (paraboloid);
}

t_paraboloid	transform_paraboloid_ws(t_paraboloid paraboloid,
					t_4x4matrix matrix)
{
	paraboloid.position = vec3_mul_mat(paraboloid.position, matrix);
	paraboloid.direction = normal_vec3(
					vec3_mul_mat(paraboloid.direction, matrix));
	paraboloid.vecr = normal_vec3(
		vec3_mul_mat(paraboloid.vecr, matrix));
	return (paraboloid);
}
