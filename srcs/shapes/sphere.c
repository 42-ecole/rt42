/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:50:39 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:50:40 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_sphere	new_sphere(double radius, t_vector truncation_vector,
	double truncation_angle)
{
	return ((t_sphere){
			.center = (t_vector){0, 0, 0, 1},
			.radius = radius,
			.radius_vector0 = (t_vector){0, 0, 1, 0},
			.radius_vector1 = (t_vector){0, 1, 0, 0},
			.truncation_vector = truncation_vector,
			.truncation_angle = truncation_angle});
}

t_sphere	move_sphere(t_sphere sphere, t_vector vector)
{
	sphere.center = sum_vec3(sphere.center, vector);
	return (sphere);
}

t_sphere	rotate_sphere_os(t_sphere sphere, double x_angle, double y_angle,
	double z_angle)
{
	t_4x4matrix	rotation;

	rotation = matrix_mul(
					matrix_mul(
						get_rotation_matrix_arbitrary(
							normal_vec3(
								cross(
									sphere.radius_vector0,
									sphere.radius_vector1)),
							x_angle),
						get_rotation_matrix_arbitrary(sphere.radius_vector1,
							y_angle)),
					get_rotation_matrix_arbitrary(sphere.radius_vector0,
						z_angle));
	sphere.radius_vector0 = vec3_mul_mat(sphere.radius_vector0, rotation);
	sphere.radius_vector1 = vec3_mul_mat(sphere.radius_vector1, rotation);
	sphere.truncation_vector = vec3_mul_mat(sphere.truncation_vector,
		rotation);
	return (sphere);
}

t_sphere	transform_sphere_ws(t_sphere sphere, t_4x4matrix matrix)
{
	sphere.center = vec3_mul_mat(sphere.center, matrix);
	sphere.radius_vector0 = normal_vec3(
		vec3_mul_mat(sphere.radius_vector0, matrix));
	sphere.radius_vector1 = normal_vec3(
		vec3_mul_mat(sphere.radius_vector1, matrix));
	sphere.truncation_vector = normal_vec3(
		vec3_mul_mat(sphere.truncation_vector, matrix));
	return (sphere);
}
