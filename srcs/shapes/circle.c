/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   circle.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:49:37 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:49:38 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_circle	new_circle(double radius)
{
	return ((t_circle){
			.plane = new_triangle(100., 100.),
			.radius = radius
			});
}

t_circle	move_circle(t_circle circle, t_vector vector)
{
	circle.plane = move_triangle(circle.plane, vector);
	return (circle);
}

t_circle	rotate_circle_os(t_circle circle, double x_angle, double y_angle,
	double z_angle)
{
	circle.plane = rotate_triangle_os(circle.plane, x_angle, y_angle, z_angle);
	return (circle);
}

t_circle	transform_circle_ws(t_circle circle, t_4x4matrix matrix)
{
	circle.plane = transform_triangle_ws(circle.plane, matrix);
	return (circle);
}
