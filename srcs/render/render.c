/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:52:51 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:52:52 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void	render_window(t_render *render)
{
	static int	pt;

	if (render->ray_path != pt)
	{
		pt = render->ray_path;
		clear_screen(render);
	}
	if (render->ray_path)
		ray_path(render);
	else
	{
		clear_screen(render);
		ray_tracing(render);
		render->render_needed = 0;
	}
	render->refresh_needed = 1;
}

int			render_thread(void *data)
{
	t_render	*render;
	uint32_t	ticks;

	ticks = 0;
	render = (t_render*)data;
	while (1)
		if (render->render_needed && SDL_TICKS_PASSED(SDL_GetTicks(), ticks))
		{
			render_window(render);
			ticks = SDL_GetTicks() + 50;
		}
}
