/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rays.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:52:16 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:52:17 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	get_rays(t_ray *rays, t_camera camera, int width, int height)
{
	int			i;
	int			n_rays;
	t_4x4matrix	rotation;

	i = -1;
	n_rays = width * height;
	rotation = matrix_mul(
			get_rotation_matrix_x(-camera.polar_angle),
			get_rotation_matrix_z(-camera.azimuthal_angle));
	while (++i < n_rays)
	{
		rays[i].origin = camera.position;
		rays[i].direction = normal_vec3(
			vec3_mul_mat(
				(t_vector){
					-(i % width - width / 2),
					camera.focus,
					-(i / width - height / 2),
					0.},
				rotation));
	}
}
