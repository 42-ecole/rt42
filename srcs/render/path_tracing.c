/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_tracing.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:52:25 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:52:26 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void	get_rays(t_ray *rays, t_camera camera, int width, int height)
{
	int			i;
	int			n_rays;
	t_4x4matrix	rotation;
	t_vector	point[2];

	i = -1;
	n_rays = width * height;
	rotation = matrix_mul(
			get_rotation_matrix_x(-camera.polar_angle),
			get_rotation_matrix_z(-camera.azimuthal_angle));
	point[0] = rand_vec_in_hemisphere((t_vector){0., 1., 0., 0.});
	point[0].y = 0.;
	point[0] = mul_vec3(point[0], camera.lens_radius);
	point[1] = sum_vec3(vec3_mul_mat(point[0], rotation),
			camera.position);
	point[0] = mul_vec3(point[0], -1);
	while (++i < n_rays)
	{
		rays[i].direction = normal_vec3(vec3_mul_mat(
			sum_vec3((t_vector){-(i % width - width / 2),
				camera.focus, -(i / width - height / 2), 0.}, point[0]),
			rotation));
		rays[i].origin = point[1];
	}
}

void		ray_path(t_render *render)
{
	int			i;

	i = -1;
	get_rays(render->rays, render->camera,
			render->window_width, render->window_height);
	while (++i < render->window_width * render->window_height &&
			render->ray_path)
	{
		render->pixels[i].color = sum_vec3(render->pixels[i].color,
				trace_path(render, render->rays[i], 5));
		render->pixels[i].divider += 1;
	}
}
