/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reflect.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:52:41 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:52:42 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_ray	reflected_ray(t_vector normal, t_ray ray, double z)
{
	t_ray	res;

	if (dot(normal, ray.direction) > 0)
		normal = mul_vec3(normal, -1.);
	res.origin = sum_vec3(ray.origin,
			mul_vec3(ray.direction, z));
	res.direction = normal_vec3(sub_vec3(
			ray.direction,
			mul_vec3(
				normal,
				2. * dot(ray.direction, normal))));
	res.origin = sum_vec3(
			res.origin,
			mul_vec3(normal, EPSILON));
	return (res);
}
