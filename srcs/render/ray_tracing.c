/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_tracing.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:52:37 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:52:38 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void	get_rays(t_ray *rays, t_camera camera, int width, int height)
{
	int			i;
	int			n_rays;
	t_4x4matrix	rotation;

	i = -1;
	n_rays = width * height;
	rotation = matrix_mul(
			get_rotation_matrix_x(-camera.polar_angle),
			get_rotation_matrix_z(-camera.azimuthal_angle));
	while (++i < n_rays)
	{
		rays[i].origin = camera.position;
		rays[i].direction = normal_vec3(
			vec3_mul_mat(
				(t_vector){
					-(i % width - width / 2),
					camera.focus,
					-(i / width - height / 2),
					0.},
				rotation));
	}
}

void		ray_tracing(t_render *render)
{
	int			i;

	i = -1;
	clear_screen(render);
	get_rays(render->rays, render->camera,
			render->window_width, render->window_height);
	while (++i < render->window_width * render->window_height &&
			render->ray_path == 0)
	{
		render->pixels[i].color = sum_vec3(
				render->pixels[i].color,
				ray_trace(render, render->rays[i], 5));
		render->pixels[i].divider += 1;
	}
}
