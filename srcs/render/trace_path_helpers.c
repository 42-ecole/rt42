/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace_path_helpers.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:52:56 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:52:57 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "path_tracing.h"

int		fits_probability(double probability, t_material *material,
	t_probability_type type)
{
	if (type == ALBEDO)
		return (probability < material->diffuse);
	else if (type == SPECULAR)
		return (probability < (material->diffuse + material->specular));
	else
		return (probability <=
				(material->diffuse + material->specular
				+ material->refraction));
}

void	albedo_ray(t_ray *ray, t_intersection intersection, t_vector *signal)
{
	t_vector	color;

	color = primitive_color(intersection.primitive, intersection.uv, 1.);
	signal->x *= color.x * intersection.primitive->material->diffuse;
	signal->y *= color.y * intersection.primitive->material->diffuse;
	signal->z *= color.z * intersection.primitive->material->diffuse;
	if (dot(ray->direction, intersection.normal) > 0.)
	{
		intersection.normal = mul_vec3(intersection.normal, -1);
		intersection.mapped_normal =
			mul_vec3(intersection.mapped_normal, -1);
	}
	ray->origin = sum_vec3(
		ray->origin, mul_vec3(ray->direction, intersection.z));
	ray->direction = normal_vec3(
		rand_vec_in_hemisphere(intersection.mapped_normal));
	ray->origin = sum_vec3(ray->origin,
		mul_vec3(intersection.normal, EPSILON));
}

double	lerps(double start, double end, double percent)
{
	double p;

	p = 1. - percent;
	return (start * p + end * percent);
}

void	specular_ray(t_ray *ray, t_intersection intersection, t_vector *signal)
{
	t_vector	color;

	color = primitive_color(intersection.primitive, intersection.uv, 1.);
	signal->x *= color.x * intersection.primitive->material->specular;
	signal->y *= color.y * intersection.primitive->material->specular;
	signal->z *= color.z * intersection.primitive->material->specular;
	*ray = reflected_ray(intersection.mapped_normal, *ray, intersection.z);
}

void	refraction_ray(t_ray *ray, t_intersection intersection,
	t_vector *signal)
{
	t_vector	color;

	color = primitive_color(intersection.primitive, intersection.uv, 1.);
	signal->x *= color.x * intersection.primitive->material->refraction;
	signal->y *= color.y * intersection.primitive->material->refraction;
	signal->z *= color.z * intersection.primitive->material->refraction;
	*ray = refracted_ray(intersection, *ray,
			intersection.primitive->material->refractive_index, 1.);
}
