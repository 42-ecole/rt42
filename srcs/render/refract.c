/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   refract.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:52:45 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:52:46 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

t_ray	refracted_ray(t_intersection inter, t_ray ray,
	double eta_t, double eta_i)
{
	double	cosi;
	double	eta;
	double	k;
	t_ray	res;

	cosi = -fmax(-1., fmin(1., dot(ray.direction, inter.normal)));
	if (cosi < 0)
	{
		inter.normal = mul_vec3(inter.normal, -1.);
		return (refracted_ray(inter, ray, eta_i, eta_t));
	}
	eta = eta_i / eta_t;
	k = 1 - eta * eta * (1 - cosi * cosi);
	if (k < 0)
		res.direction = (t_vector){0., 0., 0., 0.};
	else
		res.direction = normal_vec3(sum_vec3(
			mul_vec3(ray.direction, eta),
			mul_vec3(inter.normal, (eta * cosi - sqrt(k)))));
	res.origin = sum_vec3(
		sum_vec3(ray.origin, mul_vec3(ray.direction, inter.z)),
		mul_vec3(res.direction, EPSILON));
	return (res);
}
