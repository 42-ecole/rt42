/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_trace.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:52:34 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:52:34 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static t_vector	result_color(t_intersection intersection, t_vector *colors,
	double *intensity)
{
	colors[2] = mul_vec3(primitive_color(intersection.primitive,
			intersection.uv, intensity[0]),
		intersection.primitive->material->diffuse);
	colors[3] = primitive_color(
			intersection.primitive, intersection.uv, intensity[1]);
	colors[4] = primitive_color(intersection.primitive, intersection.uv,
			intersection.primitive->material->light_intensity);
	return (sum_vec3(
				sum_vec3(
					sum_vec3(colors[0], colors[1]),
					sum_vec3(colors[2], colors[3])),
				colors[4]));
}

t_vector		ray_trace(t_render *render, t_ray ray, size_t depth)
{
	t_intersection	intersection;
	t_ray			sub_ray;
	t_vector		colors[5];
	double			intensity[2];

	colors[0] = (t_vector){0., 0., 0., 0.};
	colors[1] = (t_vector){0., 0., 0., 0.};
	intersection = scene_intersection(render->scene, ray);
	if (intersection.z < 0. || intersection.z == 1. / 0.)
		return (colors[1]);
	get_lighting(ray, intersection, render, intensity);
	if (depth)
	{
		sub_ray = reflected_ray(intersection.normal, ray, intersection.z);
		if (intersection.primitive->material->specular > 0.)
			colors[0] = mul_vec3(ray_trace(render, sub_ray, depth - 1),
				intersection.primitive->material->specular);
		sub_ray = refracted_ray(intersection, ray,
			intersection.primitive->material->refractive_index, 1.);
		if (intersection.primitive->material->refraction > 0.)
			colors[1] = mul_vec3(ray_trace(render, sub_ray, depth - 1),
				intersection.primitive->material->refraction);
	}
	return (result_color(intersection, colors, intensity));
}
