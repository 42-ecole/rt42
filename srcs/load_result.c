/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_result.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:48:05 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:48:05 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include "rt.h"

void	load_result(t_render *render)
{
	int	fd;
	int tmp;

	fd = open("intermediate.rt_save", O_RDWR);
	if (fd != -1)
	{
		read(fd, &tmp, sizeof(int));
		if (tmp == render->window_width)
		{
			read(fd, &tmp, sizeof(int));
			if (tmp == render->window_height)
			{
				read(fd, &render->camera, sizeof(t_camera));
				read(fd, render->pixels, sizeof(t_pixel) *
					(size_t)render->window_width *
					(size_t)render->window_height);
			}
		}
		close(fd);
	}
}
