/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:51:53 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:51:53 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

double			rand_double(void)
{
	return ((double)rand() / (double)RAND_MAX);
}

t_vector		rand_vec_in_hemisphere(t_vector normal)
{
	t_vector	res;

	res = (t_vector){
		.x = rand_double() - .5,
		.y = rand_double() - .5,
		.z = rand_double() - .5,
		.w = 0.};
	if (dot(res, normal) < 0)
		res = mul_vec3(res, -1.);
	return (res);
}

t_intersection	new_intersection(void)
{
	return ((t_intersection){
			.primitive = NULL,
			.normal = (t_vector){0., 0., 0., 0.},
			.uv = (t_uv){0., 0.},
			.z = 1. / 0.});
}

t_material		normalize_material(t_material material)
{
	double	l;

	if (material.diffuse + material.specular +
			material.refraction + material.emittance <= 1.0)
		return (material);
	l = sqrt(material.diffuse * material.diffuse +
			material.specular * material.specular +
			material.refraction * material.refraction +
			material.emittance * material.emittance);
	material.diffuse /= l;
	material.specular /= l;
	material.refraction /= l;
	material.emittance /= l;
	material.diffuse *= material.diffuse;
	material.specular *= material.specular;
	material.refraction *= material.refraction;
	material.emittance *= material.emittance;
	return (material);
}

double			limit(double num, double bottom, double upper)
{
	if (num < bottom)
		return (bottom);
	else if (num > upper)
		return (upper);
	return (num);
}
