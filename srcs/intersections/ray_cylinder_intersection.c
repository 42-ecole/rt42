/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_cylinder_intersection.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:45:43 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:45:43 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void		set_normal_and_uv(t_ray ray, t_cylinder cylinder, double t,
	t_intersection *res)
{
	t_vector	point;
	t_vector	position_at_height;
	double		angle;
	double		height;

	point = sum_vec3(ray.origin,
			mul_vec3(ray.direction, t));
	angle = dot(
			normal_vec3(sub_vec3(point, cylinder.position)),
			cylinder.direction);
	height = norm(sub_vec3(point, cylinder.position)) * angle;
	position_at_height = sum_vec3(cylinder.position,
		mul_vec3(cylinder.direction, height));
	res->normal = normal_vec3(sub_vec3(position_at_height, point));
	res->uv.v = height / cylinder.height;
	angle = acos(dot(res->normal, normal_vec3(cylinder.radius_vector)));
	if (dot(res->normal,
			cross(cylinder.radius_vector, cylinder.direction)) < 0)
		angle = angle + (M_PI - angle) * 2;
	res->uv.u = angle / 2. / M_PI;
}

static int		point_too_far(t_ray ray, double t, t_cylinder cyl)
{
	t_vector	point;
	t_vector	center;
	double		half_height;

	half_height = cyl.height / 2.;
	point = sum_vec3(ray.origin, mul_vec3(ray.direction, t));
	center = sum_vec3(cyl.position,
			mul_vec3(cyl.direction, half_height));
	return (norm2(sub_vec3(point, center)) >
		(cyl.radius * cyl.radius + half_height * half_height));
}

t_intersection	simplified_ray_cylinder_intersection(t_ray ray, t_cylinder cyl)
{
	t_intersection			res;
	t_vector				xy[2];
	double					t[3];
	t_quadratic_equation	eq;

	xy[0] = cross(sub_vec3(ray.origin, cyl.position), cyl.direction);
	xy[1] = cross(ray.direction, cyl.direction);
	t[2] = dot(cyl.direction, cyl.direction) * cyl.radius * cyl.radius;
	eq.a = dot(xy[1], xy[1]);
	eq.b = 2 * dot(xy[0], xy[1]);
	eq.c = dot(xy[0], xy[0]) - t[2];
	res = new_intersection();
	if (solve_quadratic_equation(eq, &t[0], &t[1]))
	{
		if (t[0] < 0. || point_too_far(ray, t[0], cyl))
			t[0] = t[1];
		if (t[0] < 0. || point_too_far(ray, t[0], cyl))
			return (res);
		res.z = t[0];
	}
	return (res);
}

t_intersection	ray_cylinder_intersection(t_ray ray, t_cylinder cyl)
{
	t_intersection			res;
	t_vector				xy[2];
	double					t[3];
	t_quadratic_equation	eq;

	xy[0] = cross(sub_vec3(ray.origin, cyl.position), cyl.direction);
	xy[1] = cross(ray.direction, cyl.direction);
	t[2] = dot(cyl.direction, cyl.direction) * cyl.radius * cyl.radius;
	eq.a = dot(xy[1], xy[1]);
	eq.b = 2 * dot(xy[0], xy[1]);
	eq.c = dot(xy[0], xy[0]) - t[2];
	res = new_intersection();
	if (solve_quadratic_equation(eq, &t[0], &t[1]))
	{
		if (t[0] < 0. || point_too_far(ray, t[0], cyl))
			t[0] = t[1];
		if (t[0] < 0. || point_too_far(ray, t[0], cyl))
			return (res);
		set_normal_and_uv(ray, cyl, t[0], &res);
		res.z = t[0];
	}
	return (res);
}
