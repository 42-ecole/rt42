/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_circle_intersection.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:45:32 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:45:33 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static t_uv		get_uv(t_vector cp, t_triangle plane, double len, double radius)
{
	t_uv	res;
	double	a0;
	double	a1;

	a0 = dot(cp, normal_vec3(sub_vec3(plane.a, plane.b)));
	a1 = dot(cp, sub_vec3(plane.a, plane.c));
	res.u = (radius - len * a0) / radius / 2.;
	res.v = (radius -
			(len * sin(acos(a0)) * (a1 < 0. ? -1. : 1.))) / 2. / radius;
	return (res);
}

t_intersection	simplified_ray_circle_intersection(t_ray ray, t_circle circle)
{
	t_intersection	res;
	t_vector		point;
	t_vector		cp;
	double			len;

	res = simplified_ray_plane_intersection(ray, circle.plane);
	point = sum_vec3(ray.origin,
				mul_vec3(ray.direction, res.z));
	cp = sub_vec3(point, circle.plane.a);
	len = norm2(cp);
	if (len > circle.radius * circle.radius)
	{
		res.z = 1. / 0.;
		return (res);
	}
	return (res);
}

t_intersection	ray_circle_intersection(t_ray ray, t_circle circle)
{
	t_intersection	res;
	t_vector		point;
	t_vector		cp;
	double			len;

	res = simplified_ray_plane_intersection(ray, circle.plane);
	point = sum_vec3(ray.origin,
				mul_vec3(ray.direction, res.z));
	cp = sub_vec3(point, circle.plane.a);
	len = norm2(cp);
	if (len > circle.radius * circle.radius)
	{
		res.z = 1. / 0.;
		return (res);
	}
	res.normal = normal_vec3(cross(
					sub_vec3(circle.plane.b, circle.plane.a),
					sub_vec3(circle.plane.c, circle.plane.a)));
	cp = normal_vec3(cp);
	res.uv = get_uv(cp, circle.plane, sqrt(len), circle.radius);
	return (res);
}
