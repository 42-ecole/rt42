/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_cone_intersection.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:45:38 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:45:39 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static int		satisfies(t_ray ray, t_cone cone, double t)
{
	t_vector	point;
	t_vector	cp;
	double		cp_norm;
	double		cos_angle;

	point = sum_vec3(ray.origin, mul_vec3(ray.direction, t));
	cp = sub_vec3(point, cone.tip);
	cp_norm = norm(cp);
	cos_angle = cos(cone.angle);
	if (dot(normal_vec3(cp), normal_vec3(cone.direction)) < 0.)
		return (0);
	if (cp_norm > cone.height / cos_angle)
		return (0);
	return (1);
}

static void		set_normal_and_uv(t_ray ray, t_cone cone, double t,
	t_intersection *res)
{
	t_vector	point;
	t_vector	v;
	double		angle;

	angle = cos(cone.angle);
	point = sum_vec3(ray.origin, mul_vec3(ray.direction, t));
	v = sum_vec3(cone.tip,
		mul_vec3(
			cone.direction,
			norm(sub_vec3(point, cone.tip)) / angle));
	res->normal = normal_vec3(sub_vec3(point, v));
	res->uv.v = norm(sub_vec3(point, cone.tip)) /
		(cone.height / angle);
	point = sum_vec3(cone.tip,
				mul_vec3(normal_vec3(sub_vec3(point, cone.tip)),
					cone.height / angle));
	v = mul_vec3(cone.direction, cone.height);
	v = normal_vec3(sub_vec3(point, sum_vec3(cone.tip, v)));
	angle = acos(dot(v, cone.radius_vector));
	if (dot(v, cross(cone.radius_vector, cone.direction)) < 0)
		angle = angle + (M_PI - angle) * 2;
	res->uv.u = angle / M_PI / 2.;
}

t_intersection	simplified_ray_cone_intersection(t_ray ray, t_cone cone)
{
	t_intersection			res;
	t_quadratic_equation	eq;
	t_vector				co;
	double					t[2];

	res = new_intersection();
	co = sub_vec3(ray.origin, cone.tip);
	eq.a = dot(ray.direction, cone.direction) *
		dot(ray.direction, cone.direction) -
		cos(cone.angle) * cos(cone.angle);
	eq.b = 2 * (dot(ray.direction, cone.direction) *
		dot(co, cone.direction) - dot(ray.direction, co) *
		cos(cone.angle) * cos(cone.angle));
	eq.c = dot(co, cone.direction) * dot(co, cone.direction) -
		dot(co, co) * cos(cone.angle) * cos(cone.angle);
	if (!solve_quadratic_equation(eq, &t[0], &t[1]))
		return (res);
	if (t[0] < 0. || !satisfies(ray, cone, t[0]))
		t[0] = t[1];
	if (t[0] < 0. || !satisfies(ray, cone, t[0]))
		return (res);
	res.z = t[0];
	return (res);
}

t_intersection	ray_cone_intersection(t_ray ray, t_cone cone)
{
	t_intersection			res;
	t_quadratic_equation	eq;
	t_vector				co;
	double					t[2];

	res = new_intersection();
	co = sub_vec3(ray.origin, cone.tip);
	eq.a = dot(ray.direction, cone.direction) *
		dot(ray.direction, cone.direction) -
		cos(cone.angle) * cos(cone.angle);
	eq.b = 2 * (dot(ray.direction, cone.direction) *
		dot(co, cone.direction) - dot(ray.direction, co) *
		cos(cone.angle) * cos(cone.angle));
	eq.c = dot(co, cone.direction) * dot(co, cone.direction) -
		dot(co, co) * cos(cone.angle) * cos(cone.angle);
	if (!solve_quadratic_equation(eq, &t[0], &t[1]))
		return (res);
	if (t[0] < 0. || !satisfies(ray, cone, t[0]))
		t[0] = t[1];
	if (t[0] < 0. || !satisfies(ray, cone, t[0]))
		return (res);
	res.z = t[0];
	set_normal_and_uv(ray, cone, t[0], &res);
	return (res);
}
