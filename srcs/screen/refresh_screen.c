/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   refresh_screen.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:53:37 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:53:38 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void		set_pixel(SDL_Surface *surface, int x, int y, int pixel)
{
	int *target_pixel;

	target_pixel = (int*)(surface->pixels +
			y * surface->pitch + x * sizeof(*target_pixel));
	*target_pixel = pixel;
}

static void	refresh_screen(t_render *render)
{
	t_vector	color;
	int			i;

	i = -1;
	render->image = SDL_GetWindowSurface(render->window);
	while (++i < render->window_width * render->window_height)
	{
		color = apply_filters(render, i);
		set_pixel(render->image,
			i % render->window_width,
			i / render->window_width,
			vec2color(color));
	}
	SDL_UpdateWindowSurface(render->window);
}

int			refresh_thread(void *data)
{
	t_render	*render;
	uint32_t	ticks;

	render = (t_render*)data;
	ticks = 0;
	while (1)
		if (render->refresh_needed && SDL_TICKS_PASSED(SDL_GetTicks(), ticks))
		{
			refresh_screen(render);
			ticks = SDL_GetTicks() + 1000;
			render->refresh_needed = 0;
		}
}
