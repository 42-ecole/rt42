/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clear_screen.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:53:32 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:53:33 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

void	clear_screen(t_render *render)
{
	int			i;
	t_vector	empty_pixel;

	i = -1;
	empty_pixel = (t_vector){0., 0., 0., 0.};
	while (++i < render->window_height * render->window_width)
	{
		render->pixels[i].color = empty_pixel;
		render->pixels[i].divider = 0;
	}
}
