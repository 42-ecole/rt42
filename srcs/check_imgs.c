/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_imgs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:49:00 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:49:01 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

int	check_imgs(t_render *render)
{
	size_t	i;

	i = -1;
	while (++i < render->n_images && render->images)
		if (render->images[i].image_data == NULL)
			return ((int)i);
	return (-1);
}
