/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdooley <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:51:47 by mdooley           #+#    #+#             */
/*   Updated: 2020/11/01 14:51:47 by mdooley          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

int			vec2color(t_vector vector)
{
	int	res;

	vector.x = vector.x > 1. ? 1. : vector.x;
	vector.y = vector.y > 1. ? 1. : vector.y;
	vector.z = vector.z > 1. ? 1. : vector.z;
	if (vector.x < 0.)
		vector.x = 0.;
	if (vector.y < 0.)
		vector.y = 0.;
	if (vector.z < 0.)
		vector.z = 0.;
	res = (((int)(vector.x * 255.)) << 16) |
		(((int)(vector.y * 255.)) << 8) |
		((int)(vector.z * 255.));
	return (res);
}

t_vector	color2vec(int color)
{
	t_vector	res;

	res.x = (double)((color >> 16) & 0xff);
	res.y = (double)((color >> 8) & 0xff);
	res.z = (double)(color & 0xff);
	res = mul_vec3(res, 1. / 255.);
	return (res);
}

t_vector	primitive_color(t_primitive *primitive, t_uv uv, double lighting)
{
	int	color;

	if (lighting > 0.)
	{
		color = get_color_uv(primitive->texture, uv);
		return (mul_vec3(color2vec(color), lighting));
	}
	return (color2vec(0x0));
}
