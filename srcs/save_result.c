/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   save_result.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:48:11 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:48:17 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include "rt.h"

void	save_result(t_render *render)
{
	int	fd;

	fd = open("intermediate.rt_save", O_TRUNC | O_WRONLY | O_CREAT,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
	if (fd != -1)
	{
		write(fd, &render->window_width, sizeof(int));
		write(fd, &render->window_height, sizeof(int));
		write(fd, &render->camera, sizeof(t_camera));
		write(fd, render->pixels, sizeof(t_pixel) *
				(size_t)render->window_width * (size_t)render->window_height);
		close(fd);
		render->last_save_time = SDL_GetTicks();
	}
}
