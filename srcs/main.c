/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:47:47 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:47:48 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rt.h"

static void	assert_err(int condition)
{
	if (condition == 0)
	{
		write(1, "error\n", sizeof("error\n") - 1);
		exit(0);
	}
}

void		init_sdl(t_render *render)
{
	render->window = SDL_CreateWindow("RT", SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, 0);
	assert_err(render->window != NULL);
	render->image = SDL_GetWindowSurface(render->window);
	render->camera.focus = (render->window_width / 2) /
		tan((render->camera.view_angle / 2.));
	render->render_needed = 1;
	SDL_SetWindowResizable(render->window, SDL_TRUE);
}

int			main(int argc, char **argv)
{
	t_render	*render;
	size_t		full_size;
	SDL_Thread	*thread;

	srand(time(NULL));
	assert_err(SDL_Init(SDL_INIT_EVERYTHING) == 0);
	full_size = WINDOW_FULLSCREEN_WIDTH * WINDOW_FULLSCREEN_HEIGHT;
	render = malloc(sizeof(t_render));
	assert_err((render->rays = malloc(sizeof(t_ray) * full_size)) != NULL);
	assert_err((render->pixels = malloc(sizeof(t_pixel) * full_size)) != NULL);
	assert_err(init(render, argc, argv) == 0);
	if (render->mode == WORKER)
		main_loop(render);
	else
	{
		init_sdl(render);
		thread = SDL_CreateThread(render_thread, NULL, (void*)render);
		SDL_DetachThread(thread);
		thread = SDL_CreateThread(refresh_thread, NULL, (void*)render);
		SDL_DetachThread(thread);
		loop(render);
	}
	return (0);
}
