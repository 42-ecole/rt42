/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parallel_light.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:47:25 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:47:26 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "light.h"

static int	obstructed(t_ray light_ray, t_scene scene, double light_distance)
{
	t_intersection	intersection;

	intersection = simplified_scene_intersection(scene, light_ray);
	if (intersection.z > 0. && intersection.z < light_distance)
	{
		return (1);
	}
	return (0);
}

void		parallel_light(t_lighting lighting, t_light light,
	double *diffuse, double *specular)
{
	t_ray	reflection;
	t_ray	light_ray;

	*diffuse = 0.;
	*specular = 0.;
	if (dot(lighting.normal, light.direction) < 0.)
		return ;
	light_ray.direction = mul_vec3(light.direction, -1.);
	light_ray.origin = sum_vec3(lighting.point,
		mul_vec3(light_ray.direction, EPSILON));
	if (obstructed(light_ray, *lighting.scene, 1. / 0.))
		return ;
	*diffuse = light.intensity * dot(lighting.normal, light.direction);
	light_ray.origin = sum_vec3(lighting.point,
		mul_vec3(light_ray.direction, 1.));
	light_ray.direction = light.direction;
	reflection = reflected_ray(lighting.intersection.normal, light_ray, 1.);
	*specular = light.intensity * powf(fmax(0., dot(reflection.direction,
		mul_vec3(lighting.view_direction, -1.))),
			lighting.specular_exponent);
}
