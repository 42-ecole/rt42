/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   directional_light.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:47:12 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:47:12 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "light.h"

void	directional_light(t_lighting lighting, t_light light,
	double *diffuse, double *specular)
{
	t_vector	direction;

	direction = normal_vec3(sub_vec3(lighting.point, light.position));
	if (acos(dot(direction, light.direction)) > light.angle)
		return ;
	point_light(lighting, light, diffuse, specular);
}
