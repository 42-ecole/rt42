/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   worker_task.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/01 14:48:21 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/01 14:48:22 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "worker_task.h"

static t_ray	get_ray(t_camera camera, int index, int width, int height)
{
	t_4x4matrix	rotation;
	t_vector	point[2];
	t_ray		ray;

	rotation = matrix_mul(
			get_rotation_matrix_x(-camera.polar_angle),
			get_rotation_matrix_z(-camera.azimuthal_angle));
	point[0] = rand_vec_in_hemisphere((t_vector){0., 1., 0., 0.});
	point[0].y = 0.;
	point[0] = mul_vec3(point[0], camera.lens_radius);
	point[1] = sum_vec3(vec3_mul_mat(point[0], rotation),
			camera.position);
	point[0] = mul_vec3(point[0], -1);
	ray.direction = normal_vec3(vec3_mul_mat(
		sum_vec3((t_vector){-(index % width - width / 2),
			camera.focus, -(index / width - height / 2), 0.}, point[0]),
		rotation));
	ray.origin = point[1];
	return (ray);
}

static int		process_sample(void *data)
{
	SDL_sem			*semaphore;
	t_thread_arg	*arg;
	t_ray			ray;
	size_t			i;

	arg = (t_thread_arg*)data;
	semaphore = arg->semaphore;
	i = -1;
	while (++i < arg->spp)
	{
		ray = get_ray(arg->render->camera, (int)arg->sample->pix_num,
				arg->render->window_width, arg->render->window_height);
		arg->sample->pixel.color = sum_vec3(arg->sample->pixel.color,
				trace_path(arg->render, ray, 20));
		arg->sample->pixel.divider += 1;
	}
	free(data);
	SDL_SemPost(semaphore);
	return (0);
}

void			worker_task(t_render *render)
{
	SDL_Thread		*thread;
	SDL_sem			*semaphore;
	size_t			i[2];
	t_thread_arg	*arg;

	i[1] = get_num_samples_in_task();
	semaphore = SDL_CreateSemaphore(PROCESSING_THREADS);
	i[0] = -1;
	while (++i[0] < i[1])
	{
		SDL_SemWait(semaphore);
		arg = (t_thread_arg*)malloc(sizeof(t_thread_arg));
		arg->render = render;
		arg->semaphore = semaphore;
		arg->sample = &render->task->samples[i[0]];
		arg->spp = render->task->meta.spp;
		thread = SDL_CreateThread(process_sample, NULL, (void*)arg);
		SDL_DetachThread(thread);
	}
	while (SDL_SemValue(semaphore) < PROCESSING_THREADS)
		;
	render->working = 0;
	SDL_DestroySemaphore(semaphore);
}
