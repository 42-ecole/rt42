# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jcremin <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/10/16 15:30:52 by rfunk             #+#    #+#              #
#    Updated: 2020/11/01 15:34:52 by jcremin          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = RT
GCC = gcc -Wall -Wextra -Werror -march=native -mtune=native -flto -Ofast -pipe -funroll-loops
INC = -I includes \
		-I $(D_SDLINC) \
		-I libs/dyad \
		-I libs/netpbm \
		-I libs/json_parser \
		-I libs/map_parser \
		-I libs/geometry \
		-I libs/filters \
		-I libs/libft \
		-I libs/network \
		-I libs/reader_writter
OBJ =
OBJ_DIR = obj

#-----------------------Sources------------------------#
	SHAPES = cylinder.c \
		cone.c \
		torus.c \
		circle.c \
		sphere.c \
		triangle.c \
		paraboloid.c \
		normal_circle.c \
		normal_cone.c \
		normal_cylinder.c \
		normal_plane.c \
		normal_sphere.c \
		normal_triangle.c \
		normal_torus.c \
		normal_paraboloid.c \
		normal_mapping.c \
		texture.c
	SHAPES_DIR = $(addprefix ./srcs/shapes/, $(SHAPES))
	OBJ += $(SHAPES_DIR:.c=.o)

	INTERSECTS = scene_intersection.c \
		ray_circle_intersection.c \
		ray_paraboloid_intersection.c \
		ray_torus_intersection.c \
		ray_cone_intersection.c \
		ray_cylinder_intersection.c \
		ray_plane_intersection.c \
		ray_sphere_intersection.c \
		ray_triangle_intersection.c \
		primitive_intersection.c
	INTERSECTS_DIR = $(addprefix ./srcs/intersections/, $(INTERSECTS))
	OBJ += $(INTERSECTS_DIR:.c=.o)

	RENDER = path_trace.c \
		trace_path_helpers.c \
		ray_trace.c \
		get_rays.c \
		path_tracing.c \
		ray_tracing.c \
		reflect.c \
		refract.c \
		render.c
	RENDER_DIR = $(addprefix ./srcs/render/, $(RENDER))
	OBJ += $(RENDER_DIR:.c=.o)

	LIGHTS = light.c \
		directional_light.c \
		parallel_light.c \
		point_light.c
	LIGHTS_DIR = $(addprefix ./srcs/lights/, $(LIGHTS))
	OBJ += $(LIGHTS_DIR:.c=.o)

	CONTROLS = controls.c
	CONTROLS_DIR = $(addprefix ./srcs/controls/, $(CONTROLS))
	OBJ += $(CONTROLS_DIR:.c=.o)

	SCREEN = refresh_screen.c \
		clear_screen.c
	SCREEN_DIR = $(addprefix ./srcs/screen/, $(SCREEN))
	OBJ += $(SCREEN_DIR:.c=.o)

	SRC = loop.c \
		check_imgs.c \
		color.c \
		helpers.c \
		init.c \
		load_result.c \
		main.c \
		worker_task.c \
		save_result.c
	SRC_DIR = $(addprefix ./srcs/, $(SRC))
	OBJ += $(SRC_DIR:.c=.o)
#------------------------------------------------------#

#---------------------Libraries-------------------------#
	GEOMETRY = change_basis_mat.c \
			get_plane_equation.c \
			identity_mat.c \
			is_vec3.c \
			mat_op.c \
			norm.c \
			point_in_triangle_2d.c \
			ray_view_mat.c \
			rotation_matrices.c \
			solve_plane.c \
			solve_quadratic_equation.c \
			transform_mat.c \
			vec3_mat_mult.c \
			vec3_op.c \
			vec3_op2.c \
			transpose_mat.c \
			invert_mat.c
	GEOMETRY_DIR = $(addprefix ./libs/geometry/, $(GEOMETRY))
	OBJ += $(GEOMETRY_DIR:.c=.o)

	JSON = element_lack.c \
		get_string.c \
		json_del.c \
		json_destroy.c \
		json_parse.c \
		json_value_by_index.c \
		json_value_by_key.c \
		new_json.c \
		parse_array.c \
		parse_bool.c \
		parse_element.c \
		parse_null.c \
		parse_number.c \
		parse_object.c \
		parse_string.c \
		skip_spaces.c
	JSON_DIR = $(addprefix ./libs/json_parser/, $(JSON))
	OBJ += $(JSON_DIR:.c=.o)

	PARSER = add_boundary.c \
			atoi_hex.c \
			destroy_objects.c \
			get_cone.c \
			get_paraboloid.c \
			get_torus.c \
			get_cylinder.c \
			get_general_properties.c \
			get_plane_triangle_circle.c \
			get_rotation_matrix.c \
			get_sphere.c \
			json2vec.c \
			process_camera.c \
			process_images.c \
			process_lights.c \
			process_map.c \
			process_materials.c \
			process_object.c \
			process_transform_primitives.c \
			process_transformation.c
	PARSER_DIR = $(addprefix ./libs/map_parser/, $(PARSER))
	OBJ += $(PARSER_DIR:.c=.o)

	NETPBM = netpbm_helpers.c \
			read_image.c \
			read_meta.c \
			read_ppm_ascii.c \
			read_ppm_binary.c
	NETPBM_DIR = $(addprefix ./libs/netpbm/, $(NETPBM))
	OBJ += $(NETPBM_DIR:.c=.o)

	FILTERS = active_filter.c \
			apply_filters.c \
			blur.c \
			get_color_from_pixel.c \
			gray.c \
			index_brightness.c \
			median.c \
			negative.c \
			sepia.c
	FILTERS_DIR = $(addprefix ./libs/filters/, $(FILTERS))
	OBJ += $(FILTERS_DIR:.c=.o)

	NETWORK = check_validity.c \
			csiphash.c \
			data_hook.c \
			hooks.c \
			get_nonce.c \
			network_init.c \
			master_data_hook.c \
			networking.c \
			receive_task.c \
			slave_data_hook.c \
			data_singleton.c \
			send_task.c \
			send_result.c \
			request_task.c \
			receive_result.c \
			get_num_samples_in_task.c
	NETWORK_DIR = $(addprefix ./libs/network/, $(NETWORK))
	OBJ += $(NETWORK_DIR:.c=.o)

	WRITE_WRAPPER = ft_fd_write_wrapper.c \
					ft_write_wrapper.c \
					read_map.c
	WRITE_WRAPPER_DIR = $(addprefix ./libs/reader_writter/, $(WRITE_WRAPPER))
	OBJ += $(WRITE_WRAPPER_DIR:.c=.o)
#-------------------------------------------------------#

#-----------------External-Libraries--------------------#
	DYAD = dyad.c
	O_DYAD = $(addprefix $(OBJ_DIR)/, $(DYAD:.c=.o))

	N_SDLARCH = SDL2-2.0.12.tar.gz
	D_SDL = $(CURDIR)/SDL2-2.0.12/
	D_SDLINC = $(D_SDL)include
	D_SDLLIB = $(D_SDL)lib
	D_OBJ = ./obj/
	L_SDL = `$(D_SDL)sdl2-config --libs`

	LIBS := -L libs/libft -lft $(L_SDL)
#-------------------------------------------------------#

#-----------------------Styles---------------------------#
	NONE = \033[0m
	INVERT := \033[7m
	GREEN := \033[32m
	RED := \033[31m
	SUCCESS := [$(GREEN)✓$(NONE)]
	SUCCESS2 := [$(INVERT)$(GREEN)✓$(NONE)]
	APPCOMPILATIONOK := $(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):$(NAME) has been successfully compiled.$(NONE)
	APPDELETED := $(INVERT)$(RED)Removed ✓$(NONE)$(INVERT):$(NAME) has been successfully removed.$(NONE)
#--------------------------------------------------------#

all: $(NAME)

$(NAME): $(D_SDL) $(D_SDLLIB) $(D_OBJ) $(OBJ) $(O_DYAD)
	@make -C libs/libft
	@$(GCC) -o $(NAME) $(OBJ) $(O_DYAD) $(INC) $(LIBS)
	@echo "$(APPCOMPILATIONOK)"

$(OBJ): %.o: %.c
	@echo -n $(NAME):' $@: '
	@$(CC) -c $(INC) $< -o $@
	@echo "$(SUCCESS)"

$(D_SDL):
	@tar -xf $(N_SDLARCH)
	@printf "RTv1:  %-25s$(CYAN)[extracted]$(NONE)\n" $@

$(D_SDLLIB):
	@mkdir $(D_SDLLIB)
	@printf "\n$(CYAN)[configuring SDL]$(NONE)\n"
	@cd $(D_SDL); ./configure --prefix=`pwd`/lib
	@printf "$(CYAN)[compiling SDL]$(NONE)\n"
	@make -C $(D_SDL)
	@make -C $(D_SDL) install >/dev/null

$(D_OBJ):
	@mkdir $(D_OBJ)
	@echo "$(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):SDL2 has been successfully compiled.$(NONE)"

$(D_OBJ)%.o: $(D_SRC)%.c
	@$(CC) -c $(INC) $< -o $@

$(O_DYAD): $(OBJ_DIR)/%.o : ./libs/dyad/%.c
	@/bin/mkdir -p $(OBJ_DIR)
	@$(GCC) -c $< $(INC) -o $@
	@echo "$(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):Dyad has been successfully compiled.$(NONE)"

clean:
	@rm -rf $(OBJ) $(OBJ_DIR)
	@make -sC libs/libft clean

fclean: clean
	@rm -f $(NAME)
	@echo "$(APPDELETED)"
	@make -sC libs/libft fclean

del: fclean
	@rm -rf $(D_SDL)
	@echo "$(INVERT)$(RED)Removed ✓$(NONE)$(INVERT):SDL2 has been successfully removed.$(NONE)"

re: fclean all
